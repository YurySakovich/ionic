import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';

import anime from 'animejs'

import { AccountService } from '../../app/services/account.service';
import { AuthService } from '../../app/services/auth.service'

@Component({
	selector: 'page-login',
	templateUrl: 'login.html'
})
export class LoginPage implements OnInit {

	constructor(public navCtrl: NavController,
		private accountService: AccountService,
		private authService: AuthService) {

	}

	ngOnInit() {
		var current = null;
		document.querySelector('#username').addEventListener('focus', function (e) {
			if (current) current.pause();
			current = anime({
				targets: 'path',
				strokeDashoffset: {
					value: 0,
					duration: 700,
					easing: 'easeOutQuart'
				},
				strokeDasharray: {
					value: '240 1386',
					duration: 700,
					easing: 'easeOutQuart'
				}
			});
		});
		document.querySelector('#password').addEventListener('focus', function (e) {
			if (current) current.pause();
			current = anime({
				targets: 'path',
				strokeDashoffset: {
					value: -336,
					duration: 700,
					easing: 'easeOutQuart'
				},
				strokeDasharray: {
					value: '240 1386',
					duration: 700,
					easing: 'easeOutQuart'
				}
			});
		});
		document.querySelector('#submit').addEventListener('focus', function (e) {
			if (current) current.pause();
			current = anime({
				targets: 'path',
				strokeDashoffset: {
					value: -730,
					duration: 700,
					easing: 'easeOutQuart'
				},
				strokeDasharray: {
					value: '530 1386',
					duration: 700,
					easing: 'easeOutQuart'
				}
			});
		});
	}

	signup(username: any, password: any) {
		if (username && password) {
			if (username.trim().length && password.trim().length)
				// this.accountService.signup({ username: username, password: password }).subscribe(res => {
				// 	console.log(res)
				// })
				this.authService.authenticateUser({username, password}).subscribe(data=>{
					if(data.success){
						this.authService.storeUserData(data.token, data.user);
					}
				})
		}
		else{
			console.log(`username and password can't be empty`)
		}
	}

}
