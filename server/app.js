const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config/db');
const passport = require('passport');
const expressSession = require('express-session');

const AccountRep = require('./repositories/account');

const app = express();

mongoose.connect(config.database);

mongoose.connection.on('connected', () => {
	console.log('Connected to database ' + config.database);
});

mongoose.connection.on('error', (err) => {
	console.log('Database error: ' + err);
});


var port = process.env.PORT || 3000;

app.use(cors());

app.use(bodyParser.json());


app.use(expressSession({ secret: 'mySecretKey' }));
app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser(function (user, done) {
	done(null, user._id);
});

passport.deserializeUser(function (id, done) {
	User.findById(id, function (err, user) {
		done(err, user);
	});
});

const router = require('express-promise-router')();
app.use(require('./routes')(router));


app.listen(port, () => {
	console.log('Server started on port ' + port);
});