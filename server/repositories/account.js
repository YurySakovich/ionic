const User = require('../models/user');
const bcrypt = require('bcryptjs');

module.exports = class Account {
	async addUser(user) {
		let salt = await bcrypt.genSaltSync(10);
		let hash = await bcrypt.hashSync(user.password, salt);
		user.password = hash;
		return (await user.save());
	}

	async isUserExist(username) {
		return (await User.findOne({ username: username }));
	}

	async getUserById(id) {
		return (await User.findById({ id: id }));
	}
}