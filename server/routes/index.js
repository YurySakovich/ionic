const AccountRep = require('../repositories/account');
const User = require('../models/user');
const jwt = require('jsonwebtoken');
const config = require('../config/db');
const bcrypt = require('bcryptjs');

module.exports = function (router) {
	router.post('/signup', async function (req, res, next) {
		let user = new User({
			username: req.body.username,
			password: req.body.password
		});
		let isUserExist = await new AccountRep().isUserExist(user.username);
		if (!isUserExist) {
			let result = await new AccountRep().addUser(user);
			if (result) {
				res.json({
					success: true,
					result
				});
			}
		}
		else {
			res.json({
				success: false,
				msg: 'user with this username already exists. please, choose another username'
			});
		}
	});



	router.post('/authenticate', async function (req, res, next) {
		let user = new User({
			username: req.body.username,
			password: req.body.password
		});
		let isUserExist = await new AccountRep().isUserExist(user.username);
		if (!isUserExist) {
			return res.json({ success: false, msg: 'User not found' });
		}
		let isMatch = await bcrypt.compareSync(user.password, isUserExist.password);
		if (isMatch) {
			const token = jwt.sign({ user }, config.secret, {
				expiresIn: 604800
			});
			res.json({
				success: true,
				token: token,
				user: {
					id: user._id,
					username: user.username
				}
			});
		} else {
			return res.json({ success: false, msg: 'Wrong password' });
		}
	});

	return router;
};